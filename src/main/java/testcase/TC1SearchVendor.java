package testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import api.SeleniumBase;
import pages.HomePage;
import pages.LoginPage;


public class TC1SearchVendor extends SeleniumBase{
	
	
	@BeforeTest
	public void setData() {
		
		testCaseName = "TC1SearchVendor";
		testDescription = "Login and get Vendor";
		testNodes = "Leads";
		author = "Dinesh";
		category = "Smoke";
		dataSheetName = "";
	}

	
	@Test
	public void searchVendor() {
		
		/*System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://acme-test.uipath.com/account/login"	);
		*/
		
		new LoginPage()
		 .enterEmail()
		 .enterPassword()
		 .clickLogin();
		 
		new HomePage()
		.clickSearchforVendor()
		.enterVendorTaxID()
		.clickSearch()
		.printVendorName();
		
		
	}

}
