package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import api.SeleniumBase;

public class HomePage extends SeleniumBase{
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH,using="//button[text()=' Vendors']") WebElement eleVendor;
	@FindBy(how = How.XPATH,using="//a[text()='Search for Vendor']") WebElement elesearchforVendor;
	
	public VendorSearch clickSearchforVendor() {
		Actions builder = new Actions(driver);
		builder.moveToElement(eleVendor).moveToElement(elesearchforVendor).click().build().perform();
		logStep("Click Search for Vendor", "pass", true);
		return new VendorSearch();
	}
	
	

}
