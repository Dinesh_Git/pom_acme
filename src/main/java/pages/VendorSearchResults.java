package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import api.SeleniumBase;

public class VendorSearchResults extends SeleniumBase {
	
	public VendorSearchResults() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH,using="//table/tbody/tr[2]/td[1]") WebElement eleSearchresult;
	
	public VendorSearchResults printVendorName() {
		String text = eleSearchresult.getText();
		System.out.println(text);
		logStep("Vendor Name displayed successfully - "+text, "pass");
		return this;
	}
	
	
}
