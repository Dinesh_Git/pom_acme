package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import api.SeleniumBase;

public class LoginPage extends SeleniumBase{
	
	public LoginPage() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID,using="email") WebElement eleEmail;
	@FindBy(how = How.ID,using="password") WebElement elePassword;
	@FindBy(how = How.ID,using="buttonLogin") WebElement eleLogin;
	
	public LoginPage enterEmail() {
		//eleEmail.sendKeys("vdinesh1@gmail.com");
		clearAndType(eleEmail,"vdinesh1@gmail.com");
		return this;
	}
	
	public LoginPage enterPassword() {
		//elePassword.sendKeys("acme123");
		clearAndType(elePassword, "acme123");
		return this;
	}
	
	public HomePage clickLogin() {
		//eleLogin.click();
		click(eleLogin);
		return new HomePage();
	}

}
