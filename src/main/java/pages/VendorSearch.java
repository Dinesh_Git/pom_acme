package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import api.SeleniumBase;

public class VendorSearch extends SeleniumBase {
	
	public VendorSearch() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID,using="vendorTaxID") WebElement eleVendorTaxID;
	@FindBy(how = How.ID,using="buttonSearch") WebElement eleSearchButton;
	
	public VendorSearch enterVendorTaxID() {
		//eleVendorTaxID.sendKeys("FR121212");
		clearAndType(eleVendorTaxID, "FR121212");
		return this;
	}
	
	public VendorSearchResults clickSearch() {
		//eleSearchButton.click();
		click(eleSearchButton);
		return new VendorSearchResults();
	}
	
	
}
